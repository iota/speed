#include <time.h>
#include <math.h>
#include <dlfcn.h>
#include <stdio.h>

static double FACTOR = 2;
static timespec reftime;
typedef int (*clock_gettime_sig)(clockid_t, timespec*);
static clock_gettime_sig clock_gettime_orig;

extern "C" int clock_gettime(clockid_t clockid, timespec *res) {
	// if initial clock hasn't been setup yet, set it up
	if (reftime.tv_sec == 0 && reftime.tv_nsec == 0) {
		// check for FACTOR environment variable
		char *factor_env = getenv("FACTOR");
		if (factor_env) {
			FACTOR = strtod(factor_env, NULL);
		}
		// store original clock_gettime
		clock_gettime_orig = (clock_gettime_sig)dlsym(RTLD_NEXT, "clock_gettime");
		// finally, call the original gettime, and return without modifying
		int status = clock_gettime_orig(clockid, &reftime);
		(*res) = reftime;
		return status;
	}

	// use original gettime to get the actual time
	int status = clock_gettime_orig(clockid, res);
	// calculate difference between initial time and current time, in nanoseconds
	long nsec_diff = (res->tv_sec - reftime.tv_sec) * 1e9 + res->tv_nsec - reftime.tv_nsec;
	// factor the difference by FACTOR
	nsec_diff *= FACTOR;
	// calculate new seconds by dividing by length of a second (implicit cast to long will remove significand)
	long new_sec = nsec_diff / 1e9;
	// calculate new nanoseconds as remainder of second division
	long new_nsec = nsec_diff % (long)1e9;

	// set fields in caller's timespec object
	res->tv_sec = new_sec;
	res->tv_nsec = new_nsec;

	return status;
}
