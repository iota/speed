# speed

(potentially) change speed in games and other software by interposing the clock_gettime function

## compiling

g++ -fPIC -shared -o speed.so speed.cpp

## usage

FACTOR=3 LD_PRELOAD=./speed.so \<application\>
